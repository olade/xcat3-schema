/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class UsesPortEntry.
 * 
 * @version $Revision$ $Date$
 */
public class UsesPortEntry implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _portName
     */
    private java.lang.String _portName;

    /**
     * Field _usesPortRecord
     */
    private xcat.state.UsesPortRecord _usesPortRecord;


      //----------------/
     //- Constructors -/
    //----------------/

    public UsesPortEntry() {
        super();
    } //-- xcat.state.UsesPortEntry()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'portName'.
     * 
     * @return String
     * @return the value of field 'portName'.
     */
    public java.lang.String getPortName()
    {
        return this._portName;
    } //-- java.lang.String getPortName() 

    /**
     * Returns the value of field 'usesPortRecord'.
     * 
     * @return UsesPortRecord
     * @return the value of field 'usesPortRecord'.
     */
    public xcat.state.UsesPortRecord getUsesPortRecord()
    {
        return this._usesPortRecord;
    } //-- xcat.state.UsesPortRecord getUsesPortRecord() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'portName'.
     * 
     * @param portName the value of field 'portName'.
     */
    public void setPortName(java.lang.String portName)
    {
        this._portName = portName;
    } //-- void setPortName(java.lang.String) 

    /**
     * Sets the value of field 'usesPortRecord'.
     * 
     * @param usesPortRecord the value of field 'usesPortRecord'.
     */
    public void setUsesPortRecord(xcat.state.UsesPortRecord usesPortRecord)
    {
        this._usesPortRecord = usesPortRecord;
    } //-- void setUsesPortRecord(xcat.state.UsesPortRecord) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.UsesPortEntry) Unmarshaller.unmarshal(xcat.state.UsesPortEntry.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

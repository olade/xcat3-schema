/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class WsPortMap.
 * 
 * @version $Revision$ $Date$
 */
public class WsPortMap implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _wsPortEntryList
     */
    private java.util.Vector _wsPortEntryList;


      //----------------/
     //- Constructors -/
    //----------------/

    public WsPortMap() {
        super();
        _wsPortEntryList = new Vector();
    } //-- xcat.state.WsPortMap()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addWsPortEntry
     * 
     * 
     * 
     * @param vWsPortEntry
     */
    public void addWsPortEntry(xcat.state.WsPortEntry vWsPortEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        _wsPortEntryList.addElement(vWsPortEntry);
    } //-- void addWsPortEntry(xcat.state.WsPortEntry) 

    /**
     * Method addWsPortEntry
     * 
     * 
     * 
     * @param index
     * @param vWsPortEntry
     */
    public void addWsPortEntry(int index, xcat.state.WsPortEntry vWsPortEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        _wsPortEntryList.insertElementAt(vWsPortEntry, index);
    } //-- void addWsPortEntry(int, xcat.state.WsPortEntry) 

    /**
     * Method enumerateWsPortEntry
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateWsPortEntry()
    {
        return _wsPortEntryList.elements();
    } //-- java.util.Enumeration enumerateWsPortEntry() 

    /**
     * Method getWsPortEntry
     * 
     * 
     * 
     * @param index
     * @return WsPortEntry
     */
    public xcat.state.WsPortEntry getWsPortEntry(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _wsPortEntryList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (xcat.state.WsPortEntry) _wsPortEntryList.elementAt(index);
    } //-- xcat.state.WsPortEntry getWsPortEntry(int) 

    /**
     * Method getWsPortEntry
     * 
     * 
     * 
     * @return WsPortEntry
     */
    public xcat.state.WsPortEntry[] getWsPortEntry()
    {
        int size = _wsPortEntryList.size();
        xcat.state.WsPortEntry[] mArray = new xcat.state.WsPortEntry[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (xcat.state.WsPortEntry) _wsPortEntryList.elementAt(index);
        }
        return mArray;
    } //-- xcat.state.WsPortEntry[] getWsPortEntry() 

    /**
     * Method getWsPortEntryCount
     * 
     * 
     * 
     * @return int
     */
    public int getWsPortEntryCount()
    {
        return _wsPortEntryList.size();
    } //-- int getWsPortEntryCount() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllWsPortEntry
     * 
     */
    public void removeAllWsPortEntry()
    {
        _wsPortEntryList.removeAllElements();
    } //-- void removeAllWsPortEntry() 

    /**
     * Method removeWsPortEntry
     * 
     * 
     * 
     * @param index
     * @return WsPortEntry
     */
    public xcat.state.WsPortEntry removeWsPortEntry(int index)
    {
        java.lang.Object obj = _wsPortEntryList.elementAt(index);
        _wsPortEntryList.removeElementAt(index);
        return (xcat.state.WsPortEntry) obj;
    } //-- xcat.state.WsPortEntry removeWsPortEntry(int) 

    /**
     * Method setWsPortEntry
     * 
     * 
     * 
     * @param index
     * @param vWsPortEntry
     */
    public void setWsPortEntry(int index, xcat.state.WsPortEntry vWsPortEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _wsPortEntryList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _wsPortEntryList.setElementAt(vWsPortEntry, index);
    } //-- void setWsPortEntry(int, xcat.state.WsPortEntry) 

    /**
     * Method setWsPortEntry
     * 
     * 
     * 
     * @param wsPortEntryArray
     */
    public void setWsPortEntry(xcat.state.WsPortEntry[] wsPortEntryArray)
    {
        //-- copy array
        _wsPortEntryList.removeAllElements();
        for (int i = 0; i < wsPortEntryArray.length; i++) {
            _wsPortEntryList.addElement(wsPortEntryArray[i]);
        }
    } //-- void setWsPortEntry(xcat.state.WsPortEntry) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.WsPortMap) Unmarshaller.unmarshal(xcat.state.WsPortMap.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

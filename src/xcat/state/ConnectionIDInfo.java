/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConnectionIDInfo.
 * 
 * @version $Revision$ $Date$
 */
public class ConnectionIDInfo implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _providerIDHandle
     */
    private java.lang.String _providerIDHandle;

    /**
     * Field _userIDHandle
     */
    private java.lang.String _userIDHandle;

    /**
     * Field _providerName
     */
    private java.lang.String _providerName;

    /**
     * Field _userName
     */
    private java.lang.String _userName;

    /**
     * Field _providerPortName
     */
    private java.lang.String _providerPortName;

    /**
     * Field _userPortName
     */
    private java.lang.String _userPortName;

    /**
     * Field _providesPortHandle
     */
    private java.lang.String _providesPortHandle;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConnectionIDInfo() {
        super();
    } //-- xcat.state.ConnectionIDInfo()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'providerIDHandle'.
     * 
     * @return String
     * @return the value of field 'providerIDHandle'.
     */
    public java.lang.String getProviderIDHandle()
    {
        return this._providerIDHandle;
    } //-- java.lang.String getProviderIDHandle() 

    /**
     * Returns the value of field 'providerName'.
     * 
     * @return String
     * @return the value of field 'providerName'.
     */
    public java.lang.String getProviderName()
    {
        return this._providerName;
    } //-- java.lang.String getProviderName() 

    /**
     * Returns the value of field 'providerPortName'.
     * 
     * @return String
     * @return the value of field 'providerPortName'.
     */
    public java.lang.String getProviderPortName()
    {
        return this._providerPortName;
    } //-- java.lang.String getProviderPortName() 

    /**
     * Returns the value of field 'providesPortHandle'.
     * 
     * @return String
     * @return the value of field 'providesPortHandle'.
     */
    public java.lang.String getProvidesPortHandle()
    {
        return this._providesPortHandle;
    } //-- java.lang.String getProvidesPortHandle() 

    /**
     * Returns the value of field 'userIDHandle'.
     * 
     * @return String
     * @return the value of field 'userIDHandle'.
     */
    public java.lang.String getUserIDHandle()
    {
        return this._userIDHandle;
    } //-- java.lang.String getUserIDHandle() 

    /**
     * Returns the value of field 'userName'.
     * 
     * @return String
     * @return the value of field 'userName'.
     */
    public java.lang.String getUserName()
    {
        return this._userName;
    } //-- java.lang.String getUserName() 

    /**
     * Returns the value of field 'userPortName'.
     * 
     * @return String
     * @return the value of field 'userPortName'.
     */
    public java.lang.String getUserPortName()
    {
        return this._userPortName;
    } //-- java.lang.String getUserPortName() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'providerIDHandle'.
     * 
     * @param providerIDHandle the value of field 'providerIDHandle'
     */
    public void setProviderIDHandle(java.lang.String providerIDHandle)
    {
        this._providerIDHandle = providerIDHandle;
    } //-- void setProviderIDHandle(java.lang.String) 

    /**
     * Sets the value of field 'providerName'.
     * 
     * @param providerName the value of field 'providerName'.
     */
    public void setProviderName(java.lang.String providerName)
    {
        this._providerName = providerName;
    } //-- void setProviderName(java.lang.String) 

    /**
     * Sets the value of field 'providerPortName'.
     * 
     * @param providerPortName the value of field 'providerPortName'
     */
    public void setProviderPortName(java.lang.String providerPortName)
    {
        this._providerPortName = providerPortName;
    } //-- void setProviderPortName(java.lang.String) 

    /**
     * Sets the value of field 'providesPortHandle'.
     * 
     * @param providesPortHandle the value of field
     * 'providesPortHandle'.
     */
    public void setProvidesPortHandle(java.lang.String providesPortHandle)
    {
        this._providesPortHandle = providesPortHandle;
    } //-- void setProvidesPortHandle(java.lang.String) 

    /**
     * Sets the value of field 'userIDHandle'.
     * 
     * @param userIDHandle the value of field 'userIDHandle'.
     */
    public void setUserIDHandle(java.lang.String userIDHandle)
    {
        this._userIDHandle = userIDHandle;
    } //-- void setUserIDHandle(java.lang.String) 

    /**
     * Sets the value of field 'userName'.
     * 
     * @param userName the value of field 'userName'.
     */
    public void setUserName(java.lang.String userName)
    {
        this._userName = userName;
    } //-- void setUserName(java.lang.String) 

    /**
     * Sets the value of field 'userPortName'.
     * 
     * @param userPortName the value of field 'userPortName'.
     */
    public void setUserPortName(java.lang.String userPortName)
    {
        this._userPortName = userPortName;
    } //-- void setUserPortName(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.ConnectionIDInfo) Unmarshaller.unmarshal(xcat.state.ConnectionIDInfo.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

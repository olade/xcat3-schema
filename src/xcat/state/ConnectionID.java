/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConnectionID.
 * 
 * @version $Revision$ $Date$
 */
public class ConnectionID implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _connectionIDInfo
     */
    private xcat.state.ConnectionIDInfo _connectionIDInfo;

    /**
     * Field _providerIntfName
     */
    private java.lang.String _providerIntfName;

    /**
     * Field _userIntfName
     */
    private java.lang.String _userIntfName;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConnectionID() {
        super();
    } //-- xcat.state.ConnectionID()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'connectionIDInfo'.
     * 
     * @return ConnectionIDInfo
     * @return the value of field 'connectionIDInfo'.
     */
    public xcat.state.ConnectionIDInfo getConnectionIDInfo()
    {
        return this._connectionIDInfo;
    } //-- xcat.state.ConnectionIDInfo getConnectionIDInfo() 

    /**
     * Returns the value of field 'providerIntfName'.
     * 
     * @return String
     * @return the value of field 'providerIntfName'.
     */
    public java.lang.String getProviderIntfName()
    {
        return this._providerIntfName;
    } //-- java.lang.String getProviderIntfName() 

    /**
     * Returns the value of field 'userIntfName'.
     * 
     * @return String
     * @return the value of field 'userIntfName'.
     */
    public java.lang.String getUserIntfName()
    {
        return this._userIntfName;
    } //-- java.lang.String getUserIntfName() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'connectionIDInfo'.
     * 
     * @param connectionIDInfo the value of field 'connectionIDInfo'
     */
    public void setConnectionIDInfo(xcat.state.ConnectionIDInfo connectionIDInfo)
    {
        this._connectionIDInfo = connectionIDInfo;
    } //-- void setConnectionIDInfo(xcat.state.ConnectionIDInfo) 

    /**
     * Sets the value of field 'providerIntfName'.
     * 
     * @param providerIntfName the value of field 'providerIntfName'
     */
    public void setProviderIntfName(java.lang.String providerIntfName)
    {
        this._providerIntfName = providerIntfName;
    } //-- void setProviderIntfName(java.lang.String) 

    /**
     * Sets the value of field 'userIntfName'.
     * 
     * @param userIntfName the value of field 'userIntfName'.
     */
    public void setUserIntfName(java.lang.String userIntfName)
    {
        this._userIntfName = userIntfName;
    } //-- void setUserIntfName(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.ConnectionID) Unmarshaller.unmarshal(xcat.state.ConnectionID.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IndividualCheckpoint.
 * 
 * @version $Revision$ $Date$
 */
public class IndividualCheckpoint implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _componentHandle
     */
    private java.lang.String _componentHandle;

    /**
     * Field _componentState
     */
    private xcat.state.ComponentState _componentState;


      //----------------/
     //- Constructors -/
    //----------------/

    public IndividualCheckpoint() {
        super();
    } //-- xcat.state.IndividualCheckpoint()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'componentHandle'.
     * 
     * @return String
     * @return the value of field 'componentHandle'.
     */
    public java.lang.String getComponentHandle()
    {
        return this._componentHandle;
    } //-- java.lang.String getComponentHandle() 

    /**
     * Returns the value of field 'componentState'.
     * 
     * @return ComponentState
     * @return the value of field 'componentState'.
     */
    public xcat.state.ComponentState getComponentState()
    {
        return this._componentState;
    } //-- xcat.state.ComponentState getComponentState() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'componentHandle'.
     * 
     * @param componentHandle the value of field 'componentHandle'.
     */
    public void setComponentHandle(java.lang.String componentHandle)
    {
        this._componentHandle = componentHandle;
    } //-- void setComponentHandle(java.lang.String) 

    /**
     * Sets the value of field 'componentState'.
     * 
     * @param componentState the value of field 'componentState'.
     */
    public void setComponentState(xcat.state.ComponentState componentState)
    {
        this._componentState = componentState;
    } //-- void setComponentState(xcat.state.ComponentState) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.IndividualCheckpoint) Unmarshaller.unmarshal(xcat.state.IndividualCheckpoint.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

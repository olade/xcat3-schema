/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.schema;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ComponentStaticInformation.
 * 
 * @version $Revision$ $Date$
 */
public class ComponentStaticInformation implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _componentInformation
     */
    private xcat.schema.ComponentInformation _componentInformation;

    /**
     * Field _executionEnvList
     */
    private java.util.Vector _executionEnvList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ComponentStaticInformation() {
        super();
        _executionEnvList = new Vector();
    } //-- xcat.schema.ComponentStaticInformation()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addExecutionEnv
     * 
     * 
     * 
     * @param vExecutionEnv
     */
    public void addExecutionEnv(xcat.schema.ExecutionEnv vExecutionEnv)
        throws java.lang.IndexOutOfBoundsException
    {
        _executionEnvList.addElement(vExecutionEnv);
    } //-- void addExecutionEnv(xcat.schema.ExecutionEnv) 

    /**
     * Method addExecutionEnv
     * 
     * 
     * 
     * @param index
     * @param vExecutionEnv
     */
    public void addExecutionEnv(int index, xcat.schema.ExecutionEnv vExecutionEnv)
        throws java.lang.IndexOutOfBoundsException
    {
        _executionEnvList.insertElementAt(vExecutionEnv, index);
    } //-- void addExecutionEnv(int, xcat.schema.ExecutionEnv) 

    /**
     * Method enumerateExecutionEnv
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateExecutionEnv()
    {
        return _executionEnvList.elements();
    } //-- java.util.Enumeration enumerateExecutionEnv() 

    /**
     * Returns the value of field 'componentInformation'.
     * 
     * @return ComponentInformation
     * @return the value of field 'componentInformation'.
     */
    public xcat.schema.ComponentInformation getComponentInformation()
    {
        return this._componentInformation;
    } //-- xcat.schema.ComponentInformation getComponentInformation() 

    /**
     * Method getExecutionEnv
     * 
     * 
     * 
     * @param index
     * @return ExecutionEnv
     */
    public xcat.schema.ExecutionEnv getExecutionEnv(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _executionEnvList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (xcat.schema.ExecutionEnv) _executionEnvList.elementAt(index);
    } //-- xcat.schema.ExecutionEnv getExecutionEnv(int) 

    /**
     * Method getExecutionEnv
     * 
     * 
     * 
     * @return ExecutionEnv
     */
    public xcat.schema.ExecutionEnv[] getExecutionEnv()
    {
        int size = _executionEnvList.size();
        xcat.schema.ExecutionEnv[] mArray = new xcat.schema.ExecutionEnv[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (xcat.schema.ExecutionEnv) _executionEnvList.elementAt(index);
        }
        return mArray;
    } //-- xcat.schema.ExecutionEnv[] getExecutionEnv() 

    /**
     * Method getExecutionEnvCount
     * 
     * 
     * 
     * @return int
     */
    public int getExecutionEnvCount()
    {
        return _executionEnvList.size();
    } //-- int getExecutionEnvCount() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllExecutionEnv
     * 
     */
    public void removeAllExecutionEnv()
    {
        _executionEnvList.removeAllElements();
    } //-- void removeAllExecutionEnv() 

    /**
     * Method removeExecutionEnv
     * 
     * 
     * 
     * @param index
     * @return ExecutionEnv
     */
    public xcat.schema.ExecutionEnv removeExecutionEnv(int index)
    {
        java.lang.Object obj = _executionEnvList.elementAt(index);
        _executionEnvList.removeElementAt(index);
        return (xcat.schema.ExecutionEnv) obj;
    } //-- xcat.schema.ExecutionEnv removeExecutionEnv(int) 

    /**
     * Sets the value of field 'componentInformation'.
     * 
     * @param componentInformation the value of field
     * 'componentInformation'.
     */
    public void setComponentInformation(xcat.schema.ComponentInformation componentInformation)
    {
        this._componentInformation = componentInformation;
    } //-- void setComponentInformation(xcat.schema.ComponentInformation) 

    /**
     * Method setExecutionEnv
     * 
     * 
     * 
     * @param index
     * @param vExecutionEnv
     */
    public void setExecutionEnv(int index, xcat.schema.ExecutionEnv vExecutionEnv)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _executionEnvList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _executionEnvList.setElementAt(vExecutionEnv, index);
    } //-- void setExecutionEnv(int, xcat.schema.ExecutionEnv) 

    /**
     * Method setExecutionEnv
     * 
     * 
     * 
     * @param executionEnvArray
     */
    public void setExecutionEnv(xcat.schema.ExecutionEnv[] executionEnvArray)
    {
        //-- copy array
        _executionEnvList.removeAllElements();
        for (int i = 0; i < executionEnvArray.length; i++) {
            _executionEnvList.addElement(executionEnvArray[i]);
        }
    } //-- void setExecutionEnv(xcat.schema.ExecutionEnv) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.schema.ComponentStaticInformation) Unmarshaller.unmarshal(xcat.schema.ComponentStaticInformation.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ProvidesPortMap.
 * 
 * @version $Revision$ $Date$
 */
public class ProvidesPortMap implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _providesPortEntryList
     */
    private java.util.Vector _providesPortEntryList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ProvidesPortMap() {
        super();
        _providesPortEntryList = new Vector();
    } //-- xcat.state.ProvidesPortMap()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addProvidesPortEntry
     * 
     * 
     * 
     * @param vProvidesPortEntry
     */
    public void addProvidesPortEntry(xcat.state.ProvidesPortEntry vProvidesPortEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        _providesPortEntryList.addElement(vProvidesPortEntry);
    } //-- void addProvidesPortEntry(xcat.state.ProvidesPortEntry) 

    /**
     * Method addProvidesPortEntry
     * 
     * 
     * 
     * @param index
     * @param vProvidesPortEntry
     */
    public void addProvidesPortEntry(int index, xcat.state.ProvidesPortEntry vProvidesPortEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        _providesPortEntryList.insertElementAt(vProvidesPortEntry, index);
    } //-- void addProvidesPortEntry(int, xcat.state.ProvidesPortEntry) 

    /**
     * Method enumerateProvidesPortEntry
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateProvidesPortEntry()
    {
        return _providesPortEntryList.elements();
    } //-- java.util.Enumeration enumerateProvidesPortEntry() 

    /**
     * Method getProvidesPortEntry
     * 
     * 
     * 
     * @param index
     * @return ProvidesPortEntry
     */
    public xcat.state.ProvidesPortEntry getProvidesPortEntry(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _providesPortEntryList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (xcat.state.ProvidesPortEntry) _providesPortEntryList.elementAt(index);
    } //-- xcat.state.ProvidesPortEntry getProvidesPortEntry(int) 

    /**
     * Method getProvidesPortEntry
     * 
     * 
     * 
     * @return ProvidesPortEntry
     */
    public xcat.state.ProvidesPortEntry[] getProvidesPortEntry()
    {
        int size = _providesPortEntryList.size();
        xcat.state.ProvidesPortEntry[] mArray = new xcat.state.ProvidesPortEntry[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (xcat.state.ProvidesPortEntry) _providesPortEntryList.elementAt(index);
        }
        return mArray;
    } //-- xcat.state.ProvidesPortEntry[] getProvidesPortEntry() 

    /**
     * Method getProvidesPortEntryCount
     * 
     * 
     * 
     * @return int
     */
    public int getProvidesPortEntryCount()
    {
        return _providesPortEntryList.size();
    } //-- int getProvidesPortEntryCount() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllProvidesPortEntry
     * 
     */
    public void removeAllProvidesPortEntry()
    {
        _providesPortEntryList.removeAllElements();
    } //-- void removeAllProvidesPortEntry() 

    /**
     * Method removeProvidesPortEntry
     * 
     * 
     * 
     * @param index
     * @return ProvidesPortEntry
     */
    public xcat.state.ProvidesPortEntry removeProvidesPortEntry(int index)
    {
        java.lang.Object obj = _providesPortEntryList.elementAt(index);
        _providesPortEntryList.removeElementAt(index);
        return (xcat.state.ProvidesPortEntry) obj;
    } //-- xcat.state.ProvidesPortEntry removeProvidesPortEntry(int) 

    /**
     * Method setProvidesPortEntry
     * 
     * 
     * 
     * @param index
     * @param vProvidesPortEntry
     */
    public void setProvidesPortEntry(int index, xcat.state.ProvidesPortEntry vProvidesPortEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _providesPortEntryList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _providesPortEntryList.setElementAt(vProvidesPortEntry, index);
    } //-- void setProvidesPortEntry(int, xcat.state.ProvidesPortEntry) 

    /**
     * Method setProvidesPortEntry
     * 
     * 
     * 
     * @param providesPortEntryArray
     */
    public void setProvidesPortEntry(xcat.state.ProvidesPortEntry[] providesPortEntryArray)
    {
        //-- copy array
        _providesPortEntryList.removeAllElements();
        for (int i = 0; i < providesPortEntryArray.length; i++) {
            _providesPortEntryList.addElement(providesPortEntryArray[i]);
        }
    } //-- void setProvidesPortEntry(xcat.state.ProvidesPortEntry) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.ProvidesPortMap) Unmarshaller.unmarshal(xcat.state.ProvidesPortMap.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

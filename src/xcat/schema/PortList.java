/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.schema;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class PortList.
 * 
 * @version $Revision$ $Date$
 */
public class PortList implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _providesPortList
     */
    private java.util.Vector _providesPortList;

    /**
     * Field _usesPortList
     */
    private java.util.Vector _usesPortList;


      //----------------/
     //- Constructors -/
    //----------------/

    public PortList() {
        super();
        _providesPortList = new Vector();
        _usesPortList = new Vector();
    } //-- xcat.schema.PortList()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addProvidesPort
     * 
     * 
     * 
     * @param vProvidesPort
     */
    public void addProvidesPort(java.lang.Object vProvidesPort)
        throws java.lang.IndexOutOfBoundsException
    {
        _providesPortList.addElement(vProvidesPort);
    } //-- void addProvidesPort(java.lang.Object) 

    /**
     * Method addProvidesPort
     * 
     * 
     * 
     * @param index
     * @param vProvidesPort
     */
    public void addProvidesPort(int index, java.lang.Object vProvidesPort)
        throws java.lang.IndexOutOfBoundsException
    {
        _providesPortList.insertElementAt(vProvidesPort, index);
    } //-- void addProvidesPort(int, java.lang.Object) 

    /**
     * Method addUsesPort
     * 
     * 
     * 
     * @param vUsesPort
     */
    public void addUsesPort(java.lang.Object vUsesPort)
        throws java.lang.IndexOutOfBoundsException
    {
        _usesPortList.addElement(vUsesPort);
    } //-- void addUsesPort(java.lang.Object) 

    /**
     * Method addUsesPort
     * 
     * 
     * 
     * @param index
     * @param vUsesPort
     */
    public void addUsesPort(int index, java.lang.Object vUsesPort)
        throws java.lang.IndexOutOfBoundsException
    {
        _usesPortList.insertElementAt(vUsesPort, index);
    } //-- void addUsesPort(int, java.lang.Object) 

    /**
     * Method enumerateProvidesPort
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateProvidesPort()
    {
        return _providesPortList.elements();
    } //-- java.util.Enumeration enumerateProvidesPort() 

    /**
     * Method enumerateUsesPort
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateUsesPort()
    {
        return _usesPortList.elements();
    } //-- java.util.Enumeration enumerateUsesPort() 

    /**
     * Method getProvidesPort
     * 
     * 
     * 
     * @param index
     * @return Object
     */
    public java.lang.Object getProvidesPort(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _providesPortList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (java.lang.Object) _providesPortList.elementAt(index);
    } //-- java.lang.Object getProvidesPort(int) 

    /**
     * Method getProvidesPort
     * 
     * 
     * 
     * @return Object
     */
    public java.lang.Object[] getProvidesPort()
    {
        int size = _providesPortList.size();
        java.lang.Object[] mArray = new java.lang.Object[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (java.lang.Object) _providesPortList.elementAt(index);
        }
        return mArray;
    } //-- java.lang.Object[] getProvidesPort() 

    /**
     * Method getProvidesPortCount
     * 
     * 
     * 
     * @return int
     */
    public int getProvidesPortCount()
    {
        return _providesPortList.size();
    } //-- int getProvidesPortCount() 

    /**
     * Method getUsesPort
     * 
     * 
     * 
     * @param index
     * @return Object
     */
    public java.lang.Object getUsesPort(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _usesPortList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (java.lang.Object) _usesPortList.elementAt(index);
    } //-- java.lang.Object getUsesPort(int) 

    /**
     * Method getUsesPort
     * 
     * 
     * 
     * @return Object
     */
    public java.lang.Object[] getUsesPort()
    {
        int size = _usesPortList.size();
        java.lang.Object[] mArray = new java.lang.Object[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (java.lang.Object) _usesPortList.elementAt(index);
        }
        return mArray;
    } //-- java.lang.Object[] getUsesPort() 

    /**
     * Method getUsesPortCount
     * 
     * 
     * 
     * @return int
     */
    public int getUsesPortCount()
    {
        return _usesPortList.size();
    } //-- int getUsesPortCount() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllProvidesPort
     * 
     */
    public void removeAllProvidesPort()
    {
        _providesPortList.removeAllElements();
    } //-- void removeAllProvidesPort() 

    /**
     * Method removeAllUsesPort
     * 
     */
    public void removeAllUsesPort()
    {
        _usesPortList.removeAllElements();
    } //-- void removeAllUsesPort() 

    /**
     * Method removeProvidesPort
     * 
     * 
     * 
     * @param index
     * @return Object
     */
    public java.lang.Object removeProvidesPort(int index)
    {
        java.lang.Object obj = _providesPortList.elementAt(index);
        _providesPortList.removeElementAt(index);
        return (java.lang.Object) obj;
    } //-- java.lang.Object removeProvidesPort(int) 

    /**
     * Method removeUsesPort
     * 
     * 
     * 
     * @param index
     * @return Object
     */
    public java.lang.Object removeUsesPort(int index)
    {
        java.lang.Object obj = _usesPortList.elementAt(index);
        _usesPortList.removeElementAt(index);
        return (java.lang.Object) obj;
    } //-- java.lang.Object removeUsesPort(int) 

    /**
     * Method setProvidesPort
     * 
     * 
     * 
     * @param index
     * @param vProvidesPort
     */
    public void setProvidesPort(int index, java.lang.Object vProvidesPort)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _providesPortList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _providesPortList.setElementAt(vProvidesPort, index);
    } //-- void setProvidesPort(int, java.lang.Object) 

    /**
     * Method setProvidesPort
     * 
     * 
     * 
     * @param providesPortArray
     */
    public void setProvidesPort(java.lang.Object[] providesPortArray)
    {
        //-- copy array
        _providesPortList.removeAllElements();
        for (int i = 0; i < providesPortArray.length; i++) {
            _providesPortList.addElement(providesPortArray[i]);
        }
    } //-- void setProvidesPort(java.lang.Object) 

    /**
     * Method setUsesPort
     * 
     * 
     * 
     * @param index
     * @param vUsesPort
     */
    public void setUsesPort(int index, java.lang.Object vUsesPort)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _usesPortList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _usesPortList.setElementAt(vUsesPort, index);
    } //-- void setUsesPort(int, java.lang.Object) 

    /**
     * Method setUsesPort
     * 
     * 
     * 
     * @param usesPortArray
     */
    public void setUsesPort(java.lang.Object[] usesPortArray)
    {
        //-- copy array
        _usesPortList.removeAllElements();
        for (int i = 0; i < usesPortArray.length; i++) {
            _usesPortList.addElement(usesPortArray[i]);
        }
    } //-- void setUsesPort(java.lang.Object) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.schema.PortList) Unmarshaller.unmarshal(xcat.schema.PortList.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class UsesPortMap.
 * 
 * @version $Revision$ $Date$
 */
public class UsesPortMap implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _usesPortEntryList
     */
    private java.util.Vector _usesPortEntryList;


      //----------------/
     //- Constructors -/
    //----------------/

    public UsesPortMap() {
        super();
        _usesPortEntryList = new Vector();
    } //-- xcat.state.UsesPortMap()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addUsesPortEntry
     * 
     * 
     * 
     * @param vUsesPortEntry
     */
    public void addUsesPortEntry(xcat.state.UsesPortEntry vUsesPortEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        _usesPortEntryList.addElement(vUsesPortEntry);
    } //-- void addUsesPortEntry(xcat.state.UsesPortEntry) 

    /**
     * Method addUsesPortEntry
     * 
     * 
     * 
     * @param index
     * @param vUsesPortEntry
     */
    public void addUsesPortEntry(int index, xcat.state.UsesPortEntry vUsesPortEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        _usesPortEntryList.insertElementAt(vUsesPortEntry, index);
    } //-- void addUsesPortEntry(int, xcat.state.UsesPortEntry) 

    /**
     * Method enumerateUsesPortEntry
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateUsesPortEntry()
    {
        return _usesPortEntryList.elements();
    } //-- java.util.Enumeration enumerateUsesPortEntry() 

    /**
     * Method getUsesPortEntry
     * 
     * 
     * 
     * @param index
     * @return UsesPortEntry
     */
    public xcat.state.UsesPortEntry getUsesPortEntry(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _usesPortEntryList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (xcat.state.UsesPortEntry) _usesPortEntryList.elementAt(index);
    } //-- xcat.state.UsesPortEntry getUsesPortEntry(int) 

    /**
     * Method getUsesPortEntry
     * 
     * 
     * 
     * @return UsesPortEntry
     */
    public xcat.state.UsesPortEntry[] getUsesPortEntry()
    {
        int size = _usesPortEntryList.size();
        xcat.state.UsesPortEntry[] mArray = new xcat.state.UsesPortEntry[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (xcat.state.UsesPortEntry) _usesPortEntryList.elementAt(index);
        }
        return mArray;
    } //-- xcat.state.UsesPortEntry[] getUsesPortEntry() 

    /**
     * Method getUsesPortEntryCount
     * 
     * 
     * 
     * @return int
     */
    public int getUsesPortEntryCount()
    {
        return _usesPortEntryList.size();
    } //-- int getUsesPortEntryCount() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllUsesPortEntry
     * 
     */
    public void removeAllUsesPortEntry()
    {
        _usesPortEntryList.removeAllElements();
    } //-- void removeAllUsesPortEntry() 

    /**
     * Method removeUsesPortEntry
     * 
     * 
     * 
     * @param index
     * @return UsesPortEntry
     */
    public xcat.state.UsesPortEntry removeUsesPortEntry(int index)
    {
        java.lang.Object obj = _usesPortEntryList.elementAt(index);
        _usesPortEntryList.removeElementAt(index);
        return (xcat.state.UsesPortEntry) obj;
    } //-- xcat.state.UsesPortEntry removeUsesPortEntry(int) 

    /**
     * Method setUsesPortEntry
     * 
     * 
     * 
     * @param index
     * @param vUsesPortEntry
     */
    public void setUsesPortEntry(int index, xcat.state.UsesPortEntry vUsesPortEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _usesPortEntryList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _usesPortEntryList.setElementAt(vUsesPortEntry, index);
    } //-- void setUsesPortEntry(int, xcat.state.UsesPortEntry) 

    /**
     * Method setUsesPortEntry
     * 
     * 
     * 
     * @param usesPortEntryArray
     */
    public void setUsesPortEntry(xcat.state.UsesPortEntry[] usesPortEntryArray)
    {
        //-- copy array
        _usesPortEntryList.removeAllElements();
        for (int i = 0; i < usesPortEntryArray.length; i++) {
            _usesPortEntryList.addElement(usesPortEntryArray[i]);
        }
    } //-- void setUsesPortEntry(xcat.state.UsesPortEntry) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.UsesPortMap) Unmarshaller.unmarshal(xcat.state.UsesPortMap.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

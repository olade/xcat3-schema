/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ProvidesPortRecord.
 * 
 * @version $Revision$ $Date$
 */
public class ProvidesPortRecord implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _portName
     */
    private java.lang.String _portName;

    /**
     * Field _portType
     */
    private java.lang.String _portType;

    /**
     * Field _properties
     */
    private xcat.state.Properties _properties;

    /**
     * Field _inUse
     */
    private boolean _inUse;

    /**
     * keeps track of state for field: _inUse
     */
    private boolean _has_inUse;

    /**
     * Field _numConnections
     */
    private int _numConnections;

    /**
     * keeps track of state for field: _numConnections
     */
    private boolean _has_numConnections;

    /**
     * Field _providesPortHandle
     */
    private java.lang.String _providesPortHandle;

    /**
     * Field _removed
     */
    private boolean _removed;

    /**
     * keeps track of state for field: _removed
     */
    private boolean _has_removed;


      //----------------/
     //- Constructors -/
    //----------------/

    public ProvidesPortRecord() {
        super();
    } //-- xcat.state.ProvidesPortRecord()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteInUse
     * 
     */
    public void deleteInUse()
    {
        this._has_inUse= false;
    } //-- void deleteInUse() 

    /**
     * Method deleteNumConnections
     * 
     */
    public void deleteNumConnections()
    {
        this._has_numConnections= false;
    } //-- void deleteNumConnections() 

    /**
     * Method deleteRemoved
     * 
     */
    public void deleteRemoved()
    {
        this._has_removed= false;
    } //-- void deleteRemoved() 

    /**
     * Returns the value of field 'inUse'.
     * 
     * @return boolean
     * @return the value of field 'inUse'.
     */
    public boolean getInUse()
    {
        return this._inUse;
    } //-- boolean getInUse() 

    /**
     * Returns the value of field 'numConnections'.
     * 
     * @return int
     * @return the value of field 'numConnections'.
     */
    public int getNumConnections()
    {
        return this._numConnections;
    } //-- int getNumConnections() 

    /**
     * Returns the value of field 'portName'.
     * 
     * @return String
     * @return the value of field 'portName'.
     */
    public java.lang.String getPortName()
    {
        return this._portName;
    } //-- java.lang.String getPortName() 

    /**
     * Returns the value of field 'portType'.
     * 
     * @return String
     * @return the value of field 'portType'.
     */
    public java.lang.String getPortType()
    {
        return this._portType;
    } //-- java.lang.String getPortType() 

    /**
     * Returns the value of field 'properties'.
     * 
     * @return Properties
     * @return the value of field 'properties'.
     */
    public xcat.state.Properties getProperties()
    {
        return this._properties;
    } //-- xcat.state.Properties getProperties() 

    /**
     * Returns the value of field 'providesPortHandle'.
     * 
     * @return String
     * @return the value of field 'providesPortHandle'.
     */
    public java.lang.String getProvidesPortHandle()
    {
        return this._providesPortHandle;
    } //-- java.lang.String getProvidesPortHandle() 

    /**
     * Returns the value of field 'removed'.
     * 
     * @return boolean
     * @return the value of field 'removed'.
     */
    public boolean getRemoved()
    {
        return this._removed;
    } //-- boolean getRemoved() 

    /**
     * Method hasInUse
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasInUse()
    {
        return this._has_inUse;
    } //-- boolean hasInUse() 

    /**
     * Method hasNumConnections
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumConnections()
    {
        return this._has_numConnections;
    } //-- boolean hasNumConnections() 

    /**
     * Method hasRemoved
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasRemoved()
    {
        return this._has_removed;
    } //-- boolean hasRemoved() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'inUse'.
     * 
     * @param inUse the value of field 'inUse'.
     */
    public void setInUse(boolean inUse)
    {
        this._inUse = inUse;
        this._has_inUse = true;
    } //-- void setInUse(boolean) 

    /**
     * Sets the value of field 'numConnections'.
     * 
     * @param numConnections the value of field 'numConnections'.
     */
    public void setNumConnections(int numConnections)
    {
        this._numConnections = numConnections;
        this._has_numConnections = true;
    } //-- void setNumConnections(int) 

    /**
     * Sets the value of field 'portName'.
     * 
     * @param portName the value of field 'portName'.
     */
    public void setPortName(java.lang.String portName)
    {
        this._portName = portName;
    } //-- void setPortName(java.lang.String) 

    /**
     * Sets the value of field 'portType'.
     * 
     * @param portType the value of field 'portType'.
     */
    public void setPortType(java.lang.String portType)
    {
        this._portType = portType;
    } //-- void setPortType(java.lang.String) 

    /**
     * Sets the value of field 'properties'.
     * 
     * @param properties the value of field 'properties'.
     */
    public void setProperties(xcat.state.Properties properties)
    {
        this._properties = properties;
    } //-- void setProperties(xcat.state.Properties) 

    /**
     * Sets the value of field 'providesPortHandle'.
     * 
     * @param providesPortHandle the value of field
     * 'providesPortHandle'.
     */
    public void setProvidesPortHandle(java.lang.String providesPortHandle)
    {
        this._providesPortHandle = providesPortHandle;
    } //-- void setProvidesPortHandle(java.lang.String) 

    /**
     * Sets the value of field 'removed'.
     * 
     * @param removed the value of field 'removed'.
     */
    public void setRemoved(boolean removed)
    {
        this._removed = removed;
        this._has_removed = true;
    } //-- void setRemoved(boolean) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.ProvidesPortRecord) Unmarshaller.unmarshal(xcat.state.ProvidesPortRecord.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

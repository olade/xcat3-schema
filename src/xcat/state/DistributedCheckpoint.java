/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DistributedCheckpoint.
 * 
 * @version $Revision$ $Date$
 */
public class DistributedCheckpoint implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _individualCheckpointList
     */
    private java.util.Vector _individualCheckpointList;


      //----------------/
     //- Constructors -/
    //----------------/

    public DistributedCheckpoint() {
        super();
        _individualCheckpointList = new Vector();
    } //-- xcat.state.DistributedCheckpoint()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addIndividualCheckpoint
     * 
     * 
     * 
     * @param vIndividualCheckpoint
     */
    public void addIndividualCheckpoint(xcat.state.IndividualCheckpoint vIndividualCheckpoint)
        throws java.lang.IndexOutOfBoundsException
    {
        _individualCheckpointList.addElement(vIndividualCheckpoint);
    } //-- void addIndividualCheckpoint(xcat.state.IndividualCheckpoint) 

    /**
     * Method addIndividualCheckpoint
     * 
     * 
     * 
     * @param index
     * @param vIndividualCheckpoint
     */
    public void addIndividualCheckpoint(int index, xcat.state.IndividualCheckpoint vIndividualCheckpoint)
        throws java.lang.IndexOutOfBoundsException
    {
        _individualCheckpointList.insertElementAt(vIndividualCheckpoint, index);
    } //-- void addIndividualCheckpoint(int, xcat.state.IndividualCheckpoint) 

    /**
     * Method enumerateIndividualCheckpoint
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateIndividualCheckpoint()
    {
        return _individualCheckpointList.elements();
    } //-- java.util.Enumeration enumerateIndividualCheckpoint() 

    /**
     * Method getIndividualCheckpoint
     * 
     * 
     * 
     * @param index
     * @return IndividualCheckpoint
     */
    public xcat.state.IndividualCheckpoint getIndividualCheckpoint(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _individualCheckpointList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (xcat.state.IndividualCheckpoint) _individualCheckpointList.elementAt(index);
    } //-- xcat.state.IndividualCheckpoint getIndividualCheckpoint(int) 

    /**
     * Method getIndividualCheckpoint
     * 
     * 
     * 
     * @return IndividualCheckpoint
     */
    public xcat.state.IndividualCheckpoint[] getIndividualCheckpoint()
    {
        int size = _individualCheckpointList.size();
        xcat.state.IndividualCheckpoint[] mArray = new xcat.state.IndividualCheckpoint[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (xcat.state.IndividualCheckpoint) _individualCheckpointList.elementAt(index);
        }
        return mArray;
    } //-- xcat.state.IndividualCheckpoint[] getIndividualCheckpoint() 

    /**
     * Method getIndividualCheckpointCount
     * 
     * 
     * 
     * @return int
     */
    public int getIndividualCheckpointCount()
    {
        return _individualCheckpointList.size();
    } //-- int getIndividualCheckpointCount() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllIndividualCheckpoint
     * 
     */
    public void removeAllIndividualCheckpoint()
    {
        _individualCheckpointList.removeAllElements();
    } //-- void removeAllIndividualCheckpoint() 

    /**
     * Method removeIndividualCheckpoint
     * 
     * 
     * 
     * @param index
     * @return IndividualCheckpoint
     */
    public xcat.state.IndividualCheckpoint removeIndividualCheckpoint(int index)
    {
        java.lang.Object obj = _individualCheckpointList.elementAt(index);
        _individualCheckpointList.removeElementAt(index);
        return (xcat.state.IndividualCheckpoint) obj;
    } //-- xcat.state.IndividualCheckpoint removeIndividualCheckpoint(int) 

    /**
     * Method setIndividualCheckpoint
     * 
     * 
     * 
     * @param index
     * @param vIndividualCheckpoint
     */
    public void setIndividualCheckpoint(int index, xcat.state.IndividualCheckpoint vIndividualCheckpoint)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _individualCheckpointList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _individualCheckpointList.setElementAt(vIndividualCheckpoint, index);
    } //-- void setIndividualCheckpoint(int, xcat.state.IndividualCheckpoint) 

    /**
     * Method setIndividualCheckpoint
     * 
     * 
     * 
     * @param individualCheckpointArray
     */
    public void setIndividualCheckpoint(xcat.state.IndividualCheckpoint[] individualCheckpointArray)
    {
        //-- copy array
        _individualCheckpointList.removeAllElements();
        for (int i = 0; i < individualCheckpointArray.length; i++) {
            _individualCheckpointList.addElement(individualCheckpointArray[i]);
        }
    } //-- void setIndividualCheckpoint(xcat.state.IndividualCheckpoint) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.DistributedCheckpoint) Unmarshaller.unmarshal(xcat.state.DistributedCheckpoint.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

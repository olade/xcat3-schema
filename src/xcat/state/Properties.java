/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Properties.
 * 
 * @version $Revision$ $Date$
 */
public class Properties implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _propertiesEntryList
     */
    private java.util.Vector _propertiesEntryList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Properties() {
        super();
        _propertiesEntryList = new Vector();
    } //-- xcat.state.Properties()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addPropertiesEntry
     * 
     * 
     * 
     * @param vPropertiesEntry
     */
    public void addPropertiesEntry(xcat.state.PropertiesEntry vPropertiesEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        _propertiesEntryList.addElement(vPropertiesEntry);
    } //-- void addPropertiesEntry(xcat.state.PropertiesEntry) 

    /**
     * Method addPropertiesEntry
     * 
     * 
     * 
     * @param index
     * @param vPropertiesEntry
     */
    public void addPropertiesEntry(int index, xcat.state.PropertiesEntry vPropertiesEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        _propertiesEntryList.insertElementAt(vPropertiesEntry, index);
    } //-- void addPropertiesEntry(int, xcat.state.PropertiesEntry) 

    /**
     * Method enumeratePropertiesEntry
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumeratePropertiesEntry()
    {
        return _propertiesEntryList.elements();
    } //-- java.util.Enumeration enumeratePropertiesEntry() 

    /**
     * Method getPropertiesEntry
     * 
     * 
     * 
     * @param index
     * @return PropertiesEntry
     */
    public xcat.state.PropertiesEntry getPropertiesEntry(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _propertiesEntryList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (xcat.state.PropertiesEntry) _propertiesEntryList.elementAt(index);
    } //-- xcat.state.PropertiesEntry getPropertiesEntry(int) 

    /**
     * Method getPropertiesEntry
     * 
     * 
     * 
     * @return PropertiesEntry
     */
    public xcat.state.PropertiesEntry[] getPropertiesEntry()
    {
        int size = _propertiesEntryList.size();
        xcat.state.PropertiesEntry[] mArray = new xcat.state.PropertiesEntry[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (xcat.state.PropertiesEntry) _propertiesEntryList.elementAt(index);
        }
        return mArray;
    } //-- xcat.state.PropertiesEntry[] getPropertiesEntry() 

    /**
     * Method getPropertiesEntryCount
     * 
     * 
     * 
     * @return int
     */
    public int getPropertiesEntryCount()
    {
        return _propertiesEntryList.size();
    } //-- int getPropertiesEntryCount() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllPropertiesEntry
     * 
     */
    public void removeAllPropertiesEntry()
    {
        _propertiesEntryList.removeAllElements();
    } //-- void removeAllPropertiesEntry() 

    /**
     * Method removePropertiesEntry
     * 
     * 
     * 
     * @param index
     * @return PropertiesEntry
     */
    public xcat.state.PropertiesEntry removePropertiesEntry(int index)
    {
        java.lang.Object obj = _propertiesEntryList.elementAt(index);
        _propertiesEntryList.removeElementAt(index);
        return (xcat.state.PropertiesEntry) obj;
    } //-- xcat.state.PropertiesEntry removePropertiesEntry(int) 

    /**
     * Method setPropertiesEntry
     * 
     * 
     * 
     * @param index
     * @param vPropertiesEntry
     */
    public void setPropertiesEntry(int index, xcat.state.PropertiesEntry vPropertiesEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _propertiesEntryList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _propertiesEntryList.setElementAt(vPropertiesEntry, index);
    } //-- void setPropertiesEntry(int, xcat.state.PropertiesEntry) 

    /**
     * Method setPropertiesEntry
     * 
     * 
     * 
     * @param propertiesEntryArray
     */
    public void setPropertiesEntry(xcat.state.PropertiesEntry[] propertiesEntryArray)
    {
        //-- copy array
        _propertiesEntryList.removeAllElements();
        for (int i = 0; i < propertiesEntryArray.length; i++) {
            _propertiesEntryList.addElement(propertiesEntryArray[i]);
        }
    } //-- void setPropertiesEntry(xcat.state.PropertiesEntry) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.Properties) Unmarshaller.unmarshal(xcat.state.Properties.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

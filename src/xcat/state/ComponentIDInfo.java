/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ComponentIDInfo.
 * 
 * @version $Revision$ $Date$
 */
public class ComponentIDInfo implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _instanceName
     */
    private java.lang.String _instanceName;

    /**
     * Field _portGSH
     */
    private java.lang.String _portGSH;


      //----------------/
     //- Constructors -/
    //----------------/

    public ComponentIDInfo() {
        super();
    } //-- xcat.state.ComponentIDInfo()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'instanceName'.
     * 
     * @return String
     * @return the value of field 'instanceName'.
     */
    public java.lang.String getInstanceName()
    {
        return this._instanceName;
    } //-- java.lang.String getInstanceName() 

    /**
     * Returns the value of field 'portGSH'.
     * 
     * @return String
     * @return the value of field 'portGSH'.
     */
    public java.lang.String getPortGSH()
    {
        return this._portGSH;
    } //-- java.lang.String getPortGSH() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'instanceName'.
     * 
     * @param instanceName the value of field 'instanceName'.
     */
    public void setInstanceName(java.lang.String instanceName)
    {
        this._instanceName = instanceName;
    } //-- void setInstanceName(java.lang.String) 

    /**
     * Sets the value of field 'portGSH'.
     * 
     * @param portGSH the value of field 'portGSH'.
     */
    public void setPortGSH(java.lang.String portGSH)
    {
        this._portGSH = portGSH;
    } //-- void setPortGSH(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.ComponentIDInfo) Unmarshaller.unmarshal(xcat.state.ComponentIDInfo.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ServicesInfo.
 * 
 * @version $Revision$ $Date$
 */
public class ServicesInfo implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _usesPortMap
     */
    private xcat.state.UsesPortMap _usesPortMap;

    /**
     * Field _providesPortMap
     */
    private xcat.state.ProvidesPortMap _providesPortMap;

    /**
     * Field _wsPortMap
     */
    private xcat.state.WsPortMap _wsPortMap;

    /**
     * Field _componentIDInfo
     */
    private xcat.state.ComponentIDInfo _componentIDInfo;

    /**
     * Field _properties
     */
    private xcat.state.Properties _properties;


      //----------------/
     //- Constructors -/
    //----------------/

    public ServicesInfo() {
        super();
    } //-- xcat.state.ServicesInfo()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'componentIDInfo'.
     * 
     * @return ComponentIDInfo
     * @return the value of field 'componentIDInfo'.
     */
    public xcat.state.ComponentIDInfo getComponentIDInfo()
    {
        return this._componentIDInfo;
    } //-- xcat.state.ComponentIDInfo getComponentIDInfo() 

    /**
     * Returns the value of field 'properties'.
     * 
     * @return Properties
     * @return the value of field 'properties'.
     */
    public xcat.state.Properties getProperties()
    {
        return this._properties;
    } //-- xcat.state.Properties getProperties() 

    /**
     * Returns the value of field 'providesPortMap'.
     * 
     * @return ProvidesPortMap
     * @return the value of field 'providesPortMap'.
     */
    public xcat.state.ProvidesPortMap getProvidesPortMap()
    {
        return this._providesPortMap;
    } //-- xcat.state.ProvidesPortMap getProvidesPortMap() 

    /**
     * Returns the value of field 'usesPortMap'.
     * 
     * @return UsesPortMap
     * @return the value of field 'usesPortMap'.
     */
    public xcat.state.UsesPortMap getUsesPortMap()
    {
        return this._usesPortMap;
    } //-- xcat.state.UsesPortMap getUsesPortMap() 

    /**
     * Returns the value of field 'wsPortMap'.
     * 
     * @return WsPortMap
     * @return the value of field 'wsPortMap'.
     */
    public xcat.state.WsPortMap getWsPortMap()
    {
        return this._wsPortMap;
    } //-- xcat.state.WsPortMap getWsPortMap() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'componentIDInfo'.
     * 
     * @param componentIDInfo the value of field 'componentIDInfo'.
     */
    public void setComponentIDInfo(xcat.state.ComponentIDInfo componentIDInfo)
    {
        this._componentIDInfo = componentIDInfo;
    } //-- void setComponentIDInfo(xcat.state.ComponentIDInfo) 

    /**
     * Sets the value of field 'properties'.
     * 
     * @param properties the value of field 'properties'.
     */
    public void setProperties(xcat.state.Properties properties)
    {
        this._properties = properties;
    } //-- void setProperties(xcat.state.Properties) 

    /**
     * Sets the value of field 'providesPortMap'.
     * 
     * @param providesPortMap the value of field 'providesPortMap'.
     */
    public void setProvidesPortMap(xcat.state.ProvidesPortMap providesPortMap)
    {
        this._providesPortMap = providesPortMap;
    } //-- void setProvidesPortMap(xcat.state.ProvidesPortMap) 

    /**
     * Sets the value of field 'usesPortMap'.
     * 
     * @param usesPortMap the value of field 'usesPortMap'.
     */
    public void setUsesPortMap(xcat.state.UsesPortMap usesPortMap)
    {
        this._usesPortMap = usesPortMap;
    } //-- void setUsesPortMap(xcat.state.UsesPortMap) 

    /**
     * Sets the value of field 'wsPortMap'.
     * 
     * @param wsPortMap the value of field 'wsPortMap'.
     */
    public void setWsPortMap(xcat.state.WsPortMap wsPortMap)
    {
        this._wsPortMap = wsPortMap;
    } //-- void setWsPortMap(xcat.state.WsPortMap) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.ServicesInfo) Unmarshaller.unmarshal(xcat.state.ServicesInfo.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

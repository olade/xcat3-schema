/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.schema;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ExecutionEnv.
 * 
 * @version $Revision$ $Date$
 */
public class ExecutionEnv implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _hostNameList
     */
    private java.util.Vector _hostNameList;

    /**
     * Field _creationProtoList
     */
    private java.util.Vector _creationProtoList;

    /**
     * Field _nameValuePairList
     */
    private java.util.Vector _nameValuePairList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ExecutionEnv() {
        super();
        _hostNameList = new Vector();
        _creationProtoList = new Vector();
        _nameValuePairList = new Vector();
    } //-- xcat.schema.ExecutionEnv()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addCreationProto
     * 
     * 
     * 
     * @param vCreationProto
     */
    public void addCreationProto(java.lang.String vCreationProto)
        throws java.lang.IndexOutOfBoundsException
    {
        _creationProtoList.addElement(vCreationProto);
    } //-- void addCreationProto(java.lang.String) 

    /**
     * Method addCreationProto
     * 
     * 
     * 
     * @param index
     * @param vCreationProto
     */
    public void addCreationProto(int index, java.lang.String vCreationProto)
        throws java.lang.IndexOutOfBoundsException
    {
        _creationProtoList.insertElementAt(vCreationProto, index);
    } //-- void addCreationProto(int, java.lang.String) 

    /**
     * Method addHostName
     * 
     * 
     * 
     * @param vHostName
     */
    public void addHostName(java.lang.String vHostName)
        throws java.lang.IndexOutOfBoundsException
    {
        _hostNameList.addElement(vHostName);
    } //-- void addHostName(java.lang.String) 

    /**
     * Method addHostName
     * 
     * 
     * 
     * @param index
     * @param vHostName
     */
    public void addHostName(int index, java.lang.String vHostName)
        throws java.lang.IndexOutOfBoundsException
    {
        _hostNameList.insertElementAt(vHostName, index);
    } //-- void addHostName(int, java.lang.String) 

    /**
     * Method addNameValuePair
     * 
     * 
     * 
     * @param vNameValuePair
     */
    public void addNameValuePair(xcat.schema.NameValuePair vNameValuePair)
        throws java.lang.IndexOutOfBoundsException
    {
        _nameValuePairList.addElement(vNameValuePair);
    } //-- void addNameValuePair(xcat.schema.NameValuePair) 

    /**
     * Method addNameValuePair
     * 
     * 
     * 
     * @param index
     * @param vNameValuePair
     */
    public void addNameValuePair(int index, xcat.schema.NameValuePair vNameValuePair)
        throws java.lang.IndexOutOfBoundsException
    {
        _nameValuePairList.insertElementAt(vNameValuePair, index);
    } //-- void addNameValuePair(int, xcat.schema.NameValuePair) 

    /**
     * Method enumerateCreationProto
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateCreationProto()
    {
        return _creationProtoList.elements();
    } //-- java.util.Enumeration enumerateCreationProto() 

    /**
     * Method enumerateHostName
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateHostName()
    {
        return _hostNameList.elements();
    } //-- java.util.Enumeration enumerateHostName() 

    /**
     * Method enumerateNameValuePair
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateNameValuePair()
    {
        return _nameValuePairList.elements();
    } //-- java.util.Enumeration enumerateNameValuePair() 

    /**
     * Method getCreationProto
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String getCreationProto(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _creationProtoList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (String)_creationProtoList.elementAt(index);
    } //-- java.lang.String getCreationProto(int) 

    /**
     * Method getCreationProto
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String[] getCreationProto()
    {
        int size = _creationProtoList.size();
        java.lang.String[] mArray = new java.lang.String[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (String)_creationProtoList.elementAt(index);
        }
        return mArray;
    } //-- java.lang.String[] getCreationProto() 

    /**
     * Method getCreationProtoCount
     * 
     * 
     * 
     * @return int
     */
    public int getCreationProtoCount()
    {
        return _creationProtoList.size();
    } //-- int getCreationProtoCount() 

    /**
     * Method getHostName
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String getHostName(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _hostNameList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (String)_hostNameList.elementAt(index);
    } //-- java.lang.String getHostName(int) 

    /**
     * Method getHostName
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String[] getHostName()
    {
        int size = _hostNameList.size();
        java.lang.String[] mArray = new java.lang.String[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (String)_hostNameList.elementAt(index);
        }
        return mArray;
    } //-- java.lang.String[] getHostName() 

    /**
     * Method getHostNameCount
     * 
     * 
     * 
     * @return int
     */
    public int getHostNameCount()
    {
        return _hostNameList.size();
    } //-- int getHostNameCount() 

    /**
     * Method getNameValuePair
     * 
     * 
     * 
     * @param index
     * @return NameValuePair
     */
    public xcat.schema.NameValuePair getNameValuePair(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _nameValuePairList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (xcat.schema.NameValuePair) _nameValuePairList.elementAt(index);
    } //-- xcat.schema.NameValuePair getNameValuePair(int) 

    /**
     * Method getNameValuePair
     * 
     * 
     * 
     * @return NameValuePair
     */
    public xcat.schema.NameValuePair[] getNameValuePair()
    {
        int size = _nameValuePairList.size();
        xcat.schema.NameValuePair[] mArray = new xcat.schema.NameValuePair[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (xcat.schema.NameValuePair) _nameValuePairList.elementAt(index);
        }
        return mArray;
    } //-- xcat.schema.NameValuePair[] getNameValuePair() 

    /**
     * Method getNameValuePairCount
     * 
     * 
     * 
     * @return int
     */
    public int getNameValuePairCount()
    {
        return _nameValuePairList.size();
    } //-- int getNameValuePairCount() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllCreationProto
     * 
     */
    public void removeAllCreationProto()
    {
        _creationProtoList.removeAllElements();
    } //-- void removeAllCreationProto() 

    /**
     * Method removeAllHostName
     * 
     */
    public void removeAllHostName()
    {
        _hostNameList.removeAllElements();
    } //-- void removeAllHostName() 

    /**
     * Method removeAllNameValuePair
     * 
     */
    public void removeAllNameValuePair()
    {
        _nameValuePairList.removeAllElements();
    } //-- void removeAllNameValuePair() 

    /**
     * Method removeCreationProto
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String removeCreationProto(int index)
    {
        java.lang.Object obj = _creationProtoList.elementAt(index);
        _creationProtoList.removeElementAt(index);
        return (String)obj;
    } //-- java.lang.String removeCreationProto(int) 

    /**
     * Method removeHostName
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String removeHostName(int index)
    {
        java.lang.Object obj = _hostNameList.elementAt(index);
        _hostNameList.removeElementAt(index);
        return (String)obj;
    } //-- java.lang.String removeHostName(int) 

    /**
     * Method removeNameValuePair
     * 
     * 
     * 
     * @param index
     * @return NameValuePair
     */
    public xcat.schema.NameValuePair removeNameValuePair(int index)
    {
        java.lang.Object obj = _nameValuePairList.elementAt(index);
        _nameValuePairList.removeElementAt(index);
        return (xcat.schema.NameValuePair) obj;
    } //-- xcat.schema.NameValuePair removeNameValuePair(int) 

    /**
     * Method setCreationProto
     * 
     * 
     * 
     * @param index
     * @param vCreationProto
     */
    public void setCreationProto(int index, java.lang.String vCreationProto)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _creationProtoList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _creationProtoList.setElementAt(vCreationProto, index);
    } //-- void setCreationProto(int, java.lang.String) 

    /**
     * Method setCreationProto
     * 
     * 
     * 
     * @param creationProtoArray
     */
    public void setCreationProto(java.lang.String[] creationProtoArray)
    {
        //-- copy array
        _creationProtoList.removeAllElements();
        for (int i = 0; i < creationProtoArray.length; i++) {
            _creationProtoList.addElement(creationProtoArray[i]);
        }
    } //-- void setCreationProto(java.lang.String) 

    /**
     * Method setHostName
     * 
     * 
     * 
     * @param index
     * @param vHostName
     */
    public void setHostName(int index, java.lang.String vHostName)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _hostNameList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _hostNameList.setElementAt(vHostName, index);
    } //-- void setHostName(int, java.lang.String) 

    /**
     * Method setHostName
     * 
     * 
     * 
     * @param hostNameArray
     */
    public void setHostName(java.lang.String[] hostNameArray)
    {
        //-- copy array
        _hostNameList.removeAllElements();
        for (int i = 0; i < hostNameArray.length; i++) {
            _hostNameList.addElement(hostNameArray[i]);
        }
    } //-- void setHostName(java.lang.String) 

    /**
     * Method setNameValuePair
     * 
     * 
     * 
     * @param index
     * @param vNameValuePair
     */
    public void setNameValuePair(int index, xcat.schema.NameValuePair vNameValuePair)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _nameValuePairList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _nameValuePairList.setElementAt(vNameValuePair, index);
    } //-- void setNameValuePair(int, xcat.schema.NameValuePair) 

    /**
     * Method setNameValuePair
     * 
     * 
     * 
     * @param nameValuePairArray
     */
    public void setNameValuePair(xcat.schema.NameValuePair[] nameValuePairArray)
    {
        //-- copy array
        _nameValuePairList.removeAllElements();
        for (int i = 0; i < nameValuePairArray.length; i++) {
            _nameValuePairList.addElement(nameValuePairArray[i]);
        }
    } //-- void setNameValuePair(xcat.schema.NameValuePair) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.schema.ExecutionEnv) Unmarshaller.unmarshal(xcat.schema.ExecutionEnv.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

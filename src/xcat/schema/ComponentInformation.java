/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.schema;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ComponentInformation.
 * 
 * @version $Revision$ $Date$
 */
public class ComponentInformation implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _uniqueID
     */
    private java.lang.String _uniqueID;

    /**
     * Field _name
     */
    private java.lang.String _name;

    /**
     * Field _authorList
     */
    private java.util.Vector _authorList;

    /**
     * Field _componentAuthorList
     */
    private java.util.Vector _componentAuthorList;

    /**
     * Field _portList
     */
    private xcat.schema.PortList _portList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ComponentInformation() {
        super();
        _authorList = new Vector();
        _componentAuthorList = new Vector();
    } //-- xcat.schema.ComponentInformation()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addAuthor
     * 
     * 
     * 
     * @param vAuthor
     */
    public void addAuthor(java.lang.String vAuthor)
        throws java.lang.IndexOutOfBoundsException
    {
        _authorList.addElement(vAuthor);
    } //-- void addAuthor(java.lang.String) 

    /**
     * Method addAuthor
     * 
     * 
     * 
     * @param index
     * @param vAuthor
     */
    public void addAuthor(int index, java.lang.String vAuthor)
        throws java.lang.IndexOutOfBoundsException
    {
        _authorList.insertElementAt(vAuthor, index);
    } //-- void addAuthor(int, java.lang.String) 

    /**
     * Method addComponentAuthor
     * 
     * 
     * 
     * @param vComponentAuthor
     */
    public void addComponentAuthor(java.lang.String vComponentAuthor)
        throws java.lang.IndexOutOfBoundsException
    {
        _componentAuthorList.addElement(vComponentAuthor);
    } //-- void addComponentAuthor(java.lang.String) 

    /**
     * Method addComponentAuthor
     * 
     * 
     * 
     * @param index
     * @param vComponentAuthor
     */
    public void addComponentAuthor(int index, java.lang.String vComponentAuthor)
        throws java.lang.IndexOutOfBoundsException
    {
        _componentAuthorList.insertElementAt(vComponentAuthor, index);
    } //-- void addComponentAuthor(int, java.lang.String) 

    /**
     * Method enumerateAuthor
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateAuthor()
    {
        return _authorList.elements();
    } //-- java.util.Enumeration enumerateAuthor() 

    /**
     * Method enumerateComponentAuthor
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateComponentAuthor()
    {
        return _componentAuthorList.elements();
    } //-- java.util.Enumeration enumerateComponentAuthor() 

    /**
     * Method getAuthor
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String getAuthor(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _authorList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (String)_authorList.elementAt(index);
    } //-- java.lang.String getAuthor(int) 

    /**
     * Method getAuthor
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String[] getAuthor()
    {
        int size = _authorList.size();
        java.lang.String[] mArray = new java.lang.String[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (String)_authorList.elementAt(index);
        }
        return mArray;
    } //-- java.lang.String[] getAuthor() 

    /**
     * Method getAuthorCount
     * 
     * 
     * 
     * @return int
     */
    public int getAuthorCount()
    {
        return _authorList.size();
    } //-- int getAuthorCount() 

    /**
     * Method getComponentAuthor
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String getComponentAuthor(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _componentAuthorList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (String)_componentAuthorList.elementAt(index);
    } //-- java.lang.String getComponentAuthor(int) 

    /**
     * Method getComponentAuthor
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String[] getComponentAuthor()
    {
        int size = _componentAuthorList.size();
        java.lang.String[] mArray = new java.lang.String[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (String)_componentAuthorList.elementAt(index);
        }
        return mArray;
    } //-- java.lang.String[] getComponentAuthor() 

    /**
     * Method getComponentAuthorCount
     * 
     * 
     * 
     * @return int
     */
    public int getComponentAuthorCount()
    {
        return _componentAuthorList.size();
    } //-- int getComponentAuthorCount() 

    /**
     * Returns the value of field 'name'.
     * 
     * @return String
     * @return the value of field 'name'.
     */
    public java.lang.String getName()
    {
        return this._name;
    } //-- java.lang.String getName() 

    /**
     * Returns the value of field 'portList'.
     * 
     * @return PortList
     * @return the value of field 'portList'.
     */
    public xcat.schema.PortList getPortList()
    {
        return this._portList;
    } //-- xcat.schema.PortList getPortList() 

    /**
     * Returns the value of field 'uniqueID'.
     * 
     * @return String
     * @return the value of field 'uniqueID'.
     */
    public java.lang.String getUniqueID()
    {
        return this._uniqueID;
    } //-- java.lang.String getUniqueID() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllAuthor
     * 
     */
    public void removeAllAuthor()
    {
        _authorList.removeAllElements();
    } //-- void removeAllAuthor() 

    /**
     * Method removeAllComponentAuthor
     * 
     */
    public void removeAllComponentAuthor()
    {
        _componentAuthorList.removeAllElements();
    } //-- void removeAllComponentAuthor() 

    /**
     * Method removeAuthor
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String removeAuthor(int index)
    {
        java.lang.Object obj = _authorList.elementAt(index);
        _authorList.removeElementAt(index);
        return (String)obj;
    } //-- java.lang.String removeAuthor(int) 

    /**
     * Method removeComponentAuthor
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String removeComponentAuthor(int index)
    {
        java.lang.Object obj = _componentAuthorList.elementAt(index);
        _componentAuthorList.removeElementAt(index);
        return (String)obj;
    } //-- java.lang.String removeComponentAuthor(int) 

    /**
     * Method setAuthor
     * 
     * 
     * 
     * @param index
     * @param vAuthor
     */
    public void setAuthor(int index, java.lang.String vAuthor)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _authorList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _authorList.setElementAt(vAuthor, index);
    } //-- void setAuthor(int, java.lang.String) 

    /**
     * Method setAuthor
     * 
     * 
     * 
     * @param authorArray
     */
    public void setAuthor(java.lang.String[] authorArray)
    {
        //-- copy array
        _authorList.removeAllElements();
        for (int i = 0; i < authorArray.length; i++) {
            _authorList.addElement(authorArray[i]);
        }
    } //-- void setAuthor(java.lang.String) 

    /**
     * Method setComponentAuthor
     * 
     * 
     * 
     * @param index
     * @param vComponentAuthor
     */
    public void setComponentAuthor(int index, java.lang.String vComponentAuthor)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _componentAuthorList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _componentAuthorList.setElementAt(vComponentAuthor, index);
    } //-- void setComponentAuthor(int, java.lang.String) 

    /**
     * Method setComponentAuthor
     * 
     * 
     * 
     * @param componentAuthorArray
     */
    public void setComponentAuthor(java.lang.String[] componentAuthorArray)
    {
        //-- copy array
        _componentAuthorList.removeAllElements();
        for (int i = 0; i < componentAuthorArray.length; i++) {
            _componentAuthorList.addElement(componentAuthorArray[i]);
        }
    } //-- void setComponentAuthor(java.lang.String) 

    /**
     * Sets the value of field 'name'.
     * 
     * @param name the value of field 'name'.
     */
    public void setName(java.lang.String name)
    {
        this._name = name;
    } //-- void setName(java.lang.String) 

    /**
     * Sets the value of field 'portList'.
     * 
     * @param portList the value of field 'portList'.
     */
    public void setPortList(xcat.schema.PortList portList)
    {
        this._portList = portList;
    } //-- void setPortList(xcat.schema.PortList) 

    /**
     * Sets the value of field 'uniqueID'.
     * 
     * @param uniqueID the value of field 'uniqueID'.
     */
    public void setUniqueID(java.lang.String uniqueID)
    {
        this._uniqueID = uniqueID;
    } //-- void setUniqueID(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.schema.ComponentInformation) Unmarshaller.unmarshal(xcat.schema.ComponentInformation.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

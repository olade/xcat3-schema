/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.schema;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class PortInfo.
 * 
 * @version $Revision$ $Date$
 */
public class PortInfo implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _portName
     */
    private java.lang.String _portName;

    /**
     * Field _portType
     */
    private java.lang.String _portType;


      //----------------/
     //- Constructors -/
    //----------------/

    public PortInfo() {
        super();
    } //-- xcat.schema.PortInfo()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'portName'.
     * 
     * @return String
     * @return the value of field 'portName'.
     */
    public java.lang.String getPortName()
    {
        return this._portName;
    } //-- java.lang.String getPortName() 

    /**
     * Returns the value of field 'portType'.
     * 
     * @return String
     * @return the value of field 'portType'.
     */
    public java.lang.String getPortType()
    {
        return this._portType;
    } //-- java.lang.String getPortType() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'portName'.
     * 
     * @param portName the value of field 'portName'.
     */
    public void setPortName(java.lang.String portName)
    {
        this._portName = portName;
    } //-- void setPortName(java.lang.String) 

    /**
     * Sets the value of field 'portType'.
     * 
     * @param portType the value of field 'portType'.
     */
    public void setPortType(java.lang.String portType)
    {
        this._portType = portType;
    } //-- void setPortType(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.schema.PortInfo) Unmarshaller.unmarshal(xcat.schema.PortInfo.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class WsPortRecord.
 * 
 * @version $Revision$ $Date$
 */
public class WsPortRecord implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _portName
     */
    private java.lang.String _portName;

    /**
     * Field _portType
     */
    private java.lang.String _portType;

    /**
     * Field _properties
     */
    private xcat.state.Properties _properties;

    /**
     * Field _inUse
     */
    private boolean _inUse;

    /**
     * keeps track of state for field: _inUse
     */
    private boolean _has_inUse;

    /**
     * Field _unregistered
     */
    private boolean _unregistered;

    /**
     * keeps track of state for field: _unregistered
     */
    private boolean _has_unregistered;

    /**
     * Field _endPointLocation
     */
    private java.lang.String _endPointLocation;


      //----------------/
     //- Constructors -/
    //----------------/

    public WsPortRecord() {
        super();
    } //-- xcat.state.WsPortRecord()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteInUse
     * 
     */
    public void deleteInUse()
    {
        this._has_inUse= false;
    } //-- void deleteInUse() 

    /**
     * Method deleteUnregistered
     * 
     */
    public void deleteUnregistered()
    {
        this._has_unregistered= false;
    } //-- void deleteUnregistered() 

    /**
     * Returns the value of field 'endPointLocation'.
     * 
     * @return String
     * @return the value of field 'endPointLocation'.
     */
    public java.lang.String getEndPointLocation()
    {
        return this._endPointLocation;
    } //-- java.lang.String getEndPointLocation() 

    /**
     * Returns the value of field 'inUse'.
     * 
     * @return boolean
     * @return the value of field 'inUse'.
     */
    public boolean getInUse()
    {
        return this._inUse;
    } //-- boolean getInUse() 

    /**
     * Returns the value of field 'portName'.
     * 
     * @return String
     * @return the value of field 'portName'.
     */
    public java.lang.String getPortName()
    {
        return this._portName;
    } //-- java.lang.String getPortName() 

    /**
     * Returns the value of field 'portType'.
     * 
     * @return String
     * @return the value of field 'portType'.
     */
    public java.lang.String getPortType()
    {
        return this._portType;
    } //-- java.lang.String getPortType() 

    /**
     * Returns the value of field 'properties'.
     * 
     * @return Properties
     * @return the value of field 'properties'.
     */
    public xcat.state.Properties getProperties()
    {
        return this._properties;
    } //-- xcat.state.Properties getProperties() 

    /**
     * Returns the value of field 'unregistered'.
     * 
     * @return boolean
     * @return the value of field 'unregistered'.
     */
    public boolean getUnregistered()
    {
        return this._unregistered;
    } //-- boolean getUnregistered() 

    /**
     * Method hasInUse
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasInUse()
    {
        return this._has_inUse;
    } //-- boolean hasInUse() 

    /**
     * Method hasUnregistered
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasUnregistered()
    {
        return this._has_unregistered;
    } //-- boolean hasUnregistered() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'endPointLocation'.
     * 
     * @param endPointLocation the value of field 'endPointLocation'
     */
    public void setEndPointLocation(java.lang.String endPointLocation)
    {
        this._endPointLocation = endPointLocation;
    } //-- void setEndPointLocation(java.lang.String) 

    /**
     * Sets the value of field 'inUse'.
     * 
     * @param inUse the value of field 'inUse'.
     */
    public void setInUse(boolean inUse)
    {
        this._inUse = inUse;
        this._has_inUse = true;
    } //-- void setInUse(boolean) 

    /**
     * Sets the value of field 'portName'.
     * 
     * @param portName the value of field 'portName'.
     */
    public void setPortName(java.lang.String portName)
    {
        this._portName = portName;
    } //-- void setPortName(java.lang.String) 

    /**
     * Sets the value of field 'portType'.
     * 
     * @param portType the value of field 'portType'.
     */
    public void setPortType(java.lang.String portType)
    {
        this._portType = portType;
    } //-- void setPortType(java.lang.String) 

    /**
     * Sets the value of field 'properties'.
     * 
     * @param properties the value of field 'properties'.
     */
    public void setProperties(xcat.state.Properties properties)
    {
        this._properties = properties;
    } //-- void setProperties(xcat.state.Properties) 

    /**
     * Sets the value of field 'unregistered'.
     * 
     * @param unregistered the value of field 'unregistered'.
     */
    public void setUnregistered(boolean unregistered)
    {
        this._unregistered = unregistered;
        this._has_unregistered = true;
    } //-- void setUnregistered(boolean) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.WsPortRecord) Unmarshaller.unmarshal(xcat.state.WsPortRecord.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

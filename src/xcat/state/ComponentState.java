/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ComponentState.
 * 
 * @version $Revision$ $Date$
 */
public class ComponentState implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _servicesInfo
     */
    private xcat.state.ServicesInfo _servicesInfo;

    /**
     * Field _serviceData
     */
    private xcat.state.ServiceData _serviceData;

    /**
     * Field _localState
     */
    private java.lang.String _localState;


      //----------------/
     //- Constructors -/
    //----------------/

    public ComponentState() {
        super();
    } //-- xcat.state.ComponentState()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'localState'.
     * 
     * @return String
     * @return the value of field 'localState'.
     */
    public java.lang.String getLocalState()
    {
        return this._localState;
    } //-- java.lang.String getLocalState() 

    /**
     * Returns the value of field 'serviceData'.
     * 
     * @return ServiceData
     * @return the value of field 'serviceData'.
     */
    public xcat.state.ServiceData getServiceData()
    {
        return this._serviceData;
    } //-- xcat.state.ServiceData getServiceData() 

    /**
     * Returns the value of field 'servicesInfo'.
     * 
     * @return ServicesInfo
     * @return the value of field 'servicesInfo'.
     */
    public xcat.state.ServicesInfo getServicesInfo()
    {
        return this._servicesInfo;
    } //-- xcat.state.ServicesInfo getServicesInfo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'localState'.
     * 
     * @param localState the value of field 'localState'.
     */
    public void setLocalState(java.lang.String localState)
    {
        this._localState = localState;
    } //-- void setLocalState(java.lang.String) 

    /**
     * Sets the value of field 'serviceData'.
     * 
     * @param serviceData the value of field 'serviceData'.
     */
    public void setServiceData(xcat.state.ServiceData serviceData)
    {
        this._serviceData = serviceData;
    } //-- void setServiceData(xcat.state.ServiceData) 

    /**
     * Sets the value of field 'servicesInfo'.
     * 
     * @param servicesInfo the value of field 'servicesInfo'.
     */
    public void setServicesInfo(xcat.state.ServicesInfo servicesInfo)
    {
        this._servicesInfo = servicesInfo;
    } //-- void setServicesInfo(xcat.state.ServicesInfo) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.ComponentState) Unmarshaller.unmarshal(xcat.state.ComponentState.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}

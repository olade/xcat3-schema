/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package xcat.state;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ServiceData.
 * 
 * @version $Revision$ $Date$
 */
public class ServiceData implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _serviceDataElementList
     */
    private java.util.Vector _serviceDataElementList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ServiceData() {
        super();
        _serviceDataElementList = new Vector();
    } //-- xcat.state.ServiceData()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addServiceDataElement
     * 
     * 
     * 
     * @param vServiceDataElement
     */
    public void addServiceDataElement(java.lang.String vServiceDataElement)
        throws java.lang.IndexOutOfBoundsException
    {
        _serviceDataElementList.addElement(vServiceDataElement);
    } //-- void addServiceDataElement(java.lang.String) 

    /**
     * Method addServiceDataElement
     * 
     * 
     * 
     * @param index
     * @param vServiceDataElement
     */
    public void addServiceDataElement(int index, java.lang.String vServiceDataElement)
        throws java.lang.IndexOutOfBoundsException
    {
        _serviceDataElementList.insertElementAt(vServiceDataElement, index);
    } //-- void addServiceDataElement(int, java.lang.String) 

    /**
     * Method enumerateServiceDataElement
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateServiceDataElement()
    {
        return _serviceDataElementList.elements();
    } //-- java.util.Enumeration enumerateServiceDataElement() 

    /**
     * Method getServiceDataElement
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String getServiceDataElement(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _serviceDataElementList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (String)_serviceDataElementList.elementAt(index);
    } //-- java.lang.String getServiceDataElement(int) 

    /**
     * Method getServiceDataElement
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String[] getServiceDataElement()
    {
        int size = _serviceDataElementList.size();
        java.lang.String[] mArray = new java.lang.String[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (String)_serviceDataElementList.elementAt(index);
        }
        return mArray;
    } //-- java.lang.String[] getServiceDataElement() 

    /**
     * Method getServiceDataElementCount
     * 
     * 
     * 
     * @return int
     */
    public int getServiceDataElementCount()
    {
        return _serviceDataElementList.size();
    } //-- int getServiceDataElementCount() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllServiceDataElement
     * 
     */
    public void removeAllServiceDataElement()
    {
        _serviceDataElementList.removeAllElements();
    } //-- void removeAllServiceDataElement() 

    /**
     * Method removeServiceDataElement
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String removeServiceDataElement(int index)
    {
        java.lang.Object obj = _serviceDataElementList.elementAt(index);
        _serviceDataElementList.removeElementAt(index);
        return (String)obj;
    } //-- java.lang.String removeServiceDataElement(int) 

    /**
     * Method setServiceDataElement
     * 
     * 
     * 
     * @param index
     * @param vServiceDataElement
     */
    public void setServiceDataElement(int index, java.lang.String vServiceDataElement)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _serviceDataElementList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _serviceDataElementList.setElementAt(vServiceDataElement, index);
    } //-- void setServiceDataElement(int, java.lang.String) 

    /**
     * Method setServiceDataElement
     * 
     * 
     * 
     * @param serviceDataElementArray
     */
    public void setServiceDataElement(java.lang.String[] serviceDataElementArray)
    {
        //-- copy array
        _serviceDataElementList.removeAllElements();
        for (int i = 0; i < serviceDataElementArray.length; i++) {
            _serviceDataElementList.addElement(serviceDataElementArray[i]);
        }
    } //-- void setServiceDataElement(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Object
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (xcat.state.ServiceData) Unmarshaller.unmarshal(xcat.state.ServiceData.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
